filmweb-api
===========

Projekt jest kontynuacją [filmweb-php](https://github.com/nSolutionsPL/filmweb-php).

Jako, iż Filmweb uruchomił API (nie)oficjalnie, z którego korzysta na urządzeniach mobilnych (np. Android/iOS).

Postanowiłem przepisać większość funkcji do PHP.

Projekt na licencji CC 3.0
[http://creativecommons.org/licenses/by/3.0/](http://creativecommons.org/licenses/by/3.0/)
